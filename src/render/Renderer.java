package render;

import model.Part;
import model.Vertex;
import raster.ImageBuffer;
import raster.ZBufferVisibility;
import solids.Solid;
import transforms.Mat4;

import java.util.List;

public class Renderer {
    private TriangleRasterizer triangleRasterizer;
    private LineRasterizer lineRasterizer;
    private ZBufferVisibility zBufferVisibility;
    private Mat4 viewMatrix;
    private Mat4 projMatrix;

    public Renderer(TriangleRasterizer triangleRasterizer, LineRasterizer lineRasterizer, ZBufferVisibility zBufferVisibility) {
        this.triangleRasterizer = triangleRasterizer;
        this.lineRasterizer = lineRasterizer;
        this.zBufferVisibility = zBufferVisibility;
    }

    public void renderScene(List<Solid> scene) {
        for (Solid solid : scene)
            render(solid);
    }

    public void render(Solid solid) {
        Mat4 transMatrix = solid.getModelMatrix().mul(viewMatrix).mul(projMatrix);

        for (Part part:solid.getpB()) {
            int start;
            switch (part.getType()) {
                case POINTS:
                    start = part.getStart();
                    for(int i = start; i < part.getCount(); i++) {
                        int x = (int)solid.getvB().get(i).getPosition().getX();
                        int y = (int)solid.getvB().get(i).getPosition().getY();
                        double z = solid.getvB().get(i).getPosition().getZ();
                        zBufferVisibility.drawPixelWithTest(x, y, z, solid.getvB().get(i).getColor());
                    }
                    break;
                case LINES:
                    start = part.getStart();

                    for(int i = 0; i < part.getCount(); i++) {
                        int indexV1 = start;
                        int indexV2 = start + 1;

                        start += 2;

                        int indexA = solid.getiB().get(indexV1);
                        int indexB = solid.getiB().get(indexV2);

                        Vertex v1 = solid.getvB().get(indexA);
                        Vertex v2 = solid.getvB().get(indexB);

                        if(solid.isUseModelMatrix()) {
                            v1 = v1.mul(transMatrix, 1);
                            v2 = v2.mul(transMatrix, 1);
                        }

                        clipLines(v1, v2);
                    }
                    break;
                case LINES_STRIP:
                    start = part.getStart();

                    for(int i = 0; i < part.getCount(); i++) {
                        if(start + 1 > part.getCount()){
                            return;
                        }

                        int indexV1 = start;
                        int indexV2 = start + 1;

                        start += 1;

                        int indexA = solid.getiB().get(indexV1);
                        int indexB = solid.getiB().get(indexV2);

                        Vertex v1 = solid.getvB().get(indexA);
                        Vertex v2 = solid.getvB().get(indexB);

                        if(solid.isUseModelMatrix()) {
                            v1 = v1.mul(transMatrix, 1);
                            v2 = v2.mul(transMatrix, 1);
                        }

                        clipLines(v1, v2);
                    }

                    break;
                case LINES_LOOP:
                    start = part.getStart();

                    for(int i = 0; i < part.getCount(); i++) {
                        int indexV1 = start;
                        int indexV2 = start + 1 % solid.getiB().size();

                        start += 1;

                        int indexA = solid.getiB().get(indexV1);
                        int indexB = solid.getiB().get(indexV2);

                        Vertex v1 = solid.getvB().get(indexA);
                        Vertex v2 = solid.getvB().get(indexB);

                        if(solid.isUseModelMatrix()) {
                            v1 = v1.mul(transMatrix, 1);
                            v2 = v2.mul(transMatrix, 1);
                        }

                        clipLines(v1, v2);
                    }
                    break;
                case TRIANGLES:
                    start = part.getStart();

                    for (int i = 0; i < part.getCount(); i++) {
                        if(start + 1 > part.getCount()){
                            return;
                        }

                        int indexV1 = start;
                        int indexV2 = start + 1;
                        int indexV3 = start + 2;

                        start += 3;

                        Vertex v1 = solid.getvB().get(solid.getiB().get(indexV1));
                        Vertex v2 = solid.getvB().get(solid.getiB().get(indexV2));
                        Vertex v3 = solid.getvB().get(solid.getiB().get(indexV3));

                        if(solid.isUseModelMatrix()) {
                            v1 = v1.mul(transMatrix, 1);
                            v2 = v2.mul(transMatrix, 1);
                            v3 = v3.mul(transMatrix, 1);
                        }

                        clipTriangle(v1, v2, v3);
                    }

                    break;
                case TRINAGLES_STRIP:
                    start = part.getStart();

                    for (int i = 0; i < part.getCount(); i++) {
                        int indexV1 = start;
                        int indexV2 = start + 1;
                        int indexV3 = start + 2;

                        start += 1;

                        Vertex v1 = solid.getvB().get(solid.getiB().get(indexV1));
                        Vertex v2 = solid.getvB().get(solid.getiB().get(indexV2));
                        Vertex v3 = solid.getvB().get(solid.getiB().get(indexV3));

                        if(solid.isUseModelMatrix()) {
                            v1 = v1.mul(transMatrix, 1);
                            v2 = v2.mul(transMatrix, 1);
                            v3 = v3.mul(transMatrix, 1);
                        }

                        clipTriangle(v1, v2, v3);
                    }
                    break;
            }
        }

    }

    private void clipTriangle(Vertex v1, Vertex v2, Vertex v3) {
        if(-v1.getPosition().getW() <= v1.getPosition().getX() && -v1.getPosition().getW() <= v1.getPosition().getY() && 0 <= v1.getPosition().getZ() &&
                v1.getPosition().getW() >= v1.getPosition().getX() && v1.getPosition().getW() >= v1.getPosition().getY() && v1.getPosition().getW() >= v1.getPosition().getZ() &&
                -v2.getPosition().getW() <= v2.getPosition().getX() && -v2.getPosition().getW() <= v2.getPosition().getY() && 0 <= v2.getPosition().getZ() &&
                v2.getPosition().getW() >= v2.getPosition().getX() && v2.getPosition().getW() >= v2.getPosition().getY() && v2.getPosition().getW() >= v2.getPosition().getZ() &&
                -v3.getPosition().getW() <= v3.getPosition().getX() && -v3.getPosition().getW() <= v3.getPosition().getY() && 0 <= v1.getPosition().getZ() &&
                v3.getPosition().getW() >= v3.getPosition().getX() && v3.getPosition().getW() >= v3.getPosition().getY() && v3.getPosition().getW() >= v3.getPosition().getZ()) {


            if (v1.getPosition().getW() < v2.getPosition().getW()) {
                Vertex change = v2;
                v2 = v1;
                v1 = change;
            }

            if (v2.getPosition().getW() < v3.getPosition().getW()) {
                Vertex change = v3;
                v3 = v2;
                v2 = change;
            }

            if (v1.getPosition().getW() < v2.getPosition().getW()) {
                Vertex change = v1;
                v1 = v2;
                v2 = change;
            }

            if (v1.getPosition().getW() < zBufferVisibility.getdBuffer().getClearValue()) {
                if (v2.getPosition().getW() < zBufferVisibility.getdBuffer().getClearValue()) {
                    double t = (v1.getPosition().getW() - zBufferVisibility.getdBuffer().getClearValue()) / (v1.getPosition().getW() - v2.getPosition().getW());
                    Vertex v2w = v1.mul(1 - t).add(v2.mul(t));

                    double t2 = (v1.getPosition().getW() - zBufferVisibility.getdBuffer().getClearValue()) / (v1.getPosition().getW() - v3.getPosition().getW());
                    Vertex v3w = v1.mul(1 - t2).add(v3.mul(t2));

                    triangleRasterizer.rasterize(v1, v2w, v3w);
                } else {
                    if (v3.getPosition().getW() < zBufferVisibility.getdBuffer().getClearValue()) {
                        double t = (v2.getPosition().getW() - zBufferVisibility.getdBuffer().getClearValue()) / (v2.getPosition().getW() - v3.getPosition().getW());
                        Vertex v2w = v2.mul(1 - t).add(v3.mul(t));

                        double t2 = (v1.getPosition().getW() - zBufferVisibility.getdBuffer().getClearValue()) / (v1.getPosition().getW() - v3.getPosition().getW());
                        Vertex v3w = v1.mul(1 - t2).add(v3.mul(t2));

                        triangleRasterizer.rasterize(v1, v2w, v3w);
                        triangleRasterizer.rasterize(v1, v2, v2w);
                    }
                }
            } else {
                triangleRasterizer.rasterize(v1, v2, v3);
            }
        }
    }

    public void clipLines(Vertex v1, Vertex v2) {
        if(-v1.getPosition().getW() <= v1.getPosition().getX() && -v1.getPosition().getW() <= v1.getPosition().getY() && 0 <= v1.getPosition().getZ() && v1.getPosition().getW() >= v1.getPosition().getX() && v1.getPosition().getW() >= v1.getPosition().getY() && v1.getPosition().getW() >= v1.getPosition().getZ() &&
                -v2.getPosition().getW() <= v2.getPosition().getX() && -v2.getPosition().getW() <= v2.getPosition().getY() && 0 <= v2.getPosition().getZ() && v2.getPosition().getW() >= v2.getPosition().getX() && v2.getPosition().getW() >= v2.getPosition().getY() && v2.getPosition().getW() >= v2.getPosition().getZ()){

            v1 = v1.dehomog(v1.getPosition().getW());
            v2 = v2.dehomog(v2.getPosition().getW());

            lineRasterizer.rasterize(v1, v2);
        }
    }

    public void setViewMatrix(Mat4 viewMatrix) {
        this.viewMatrix = viewMatrix;
    }

    public void setProjMatrix(Mat4 projMatrix) {
        this.projMatrix = projMatrix;
    }
}
