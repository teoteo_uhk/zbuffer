package render;

import model.Shader;
import model.Vertex;
import raster.ImageBuffer;
import raster.ZBufferVisibility;
import transforms.Col;
import transforms.Point3D;
import transforms.Vec3D;

import java.awt.*;
import java.util.Collections;

public class TriangleRasterizer {
    private ZBufferVisibility zBuffer;
    private int width, heigth;
    private Shader shader;
    private LineRasterizer lineRasterizer;
    private boolean fill;

    public TriangleRasterizer(ZBufferVisibility zBuffer, Shader shader, LineRasterizer lineRasterizer) {
        this.zBuffer = zBuffer;
        this.width = zBuffer.getiBuffer().getWidth();
        this.heigth = zBuffer.getiBuffer().getHeight();
        this.lineRasterizer = lineRasterizer;
        this.shader = shader;
        this.fill = true;
    }

    public TriangleRasterizer(ZBufferVisibility zBuffer, Shader shader, LineRasterizer lineRasterizer, boolean fill) {
        this.zBuffer = zBuffer;
        this.width = zBuffer.getiBuffer().getWidth();
        this.heigth = zBuffer.getiBuffer().getHeight();
        this.lineRasterizer = lineRasterizer;
        this.shader = shader;
        this.fill = fill;
    }

    public void rasterize(Vertex v1, Vertex v2, Vertex v3) {
        if(v1.getPosition().getZ() < 0 || v2.getPosition().getZ() < 0 || v3.getPosition().getZ() < 0){
            return;
        }

        v1 = v1.dehomog(v1.getPosition().getW());
        v2 = v2.dehomog(v2.getPosition().getW());
        v3 = v3.dehomog(v3.getPosition().getW());


        Vec3D a = v1.getPosition().ignoreW()
                .mul(new Vec3D(1, -1, 1))
                .add(new Vec3D(1, 1, 0))
                .mul(new Vec3D((width - 1) / 2., (heigth - 1) / 2., 1));

        Vec3D b = v2.getPosition().ignoreW()
                .mul(new Vec3D(1, -1, 1))
                .add(new Vec3D(1, 1, 0))
                .mul(new Vec3D((width - 1) / 2., (heigth - 1) / 2., 1));

        Vec3D c = v3.getPosition().ignoreW()
                .mul(new Vec3D(1, -1, 1))
                .add(new Vec3D(1, 1, 0))
                .mul(new Vec3D((width - 1) / 2., (heigth - 1) / 2., 1));

        if(fill) {
            if (a.getY() > b.getY()) {
                Vec3D change = a;
                a = b;
                b = change;

                Vertex changeV = v1;
                v1 = v2;
                v2 = changeV;
            }

            if (b.getY() > c.getY()) {
                Vec3D change = b;
                b = c;
                c = change;

                Vertex changeV = v2;
                v2 = v3;
                v3 = changeV;
            }

            if (a.getY() > b.getY()) {
                Vec3D change = a;
                a = b;
                b = change;

                Vertex changeV = v1;
                v1 = v2;
                v2 = changeV;
            }

            for (int y = (int) a.getY() + 1; y < (int) b.getY(); y++) {
                double s1 = (y - a.getY()) / (b.getY() - a.getY());
                double x1 = (int) (a.getX() * (1 - s1) + b.getX() * s1);

                Vertex v12 = v1.mul(1 - s1).add(v2.mul(s1));

                double s2 = (y - a.getY()) / (c.getY() - a.getY());
                double x2 = (int) (a.getX() * (1 - s2) + c.getX() * s2);
                Vertex v13 = v1.mul(1 - s2).add(v3.mul(s2));


                if (x1 > x2) {
                    double change = x1;
                    x1 = x2;
                    x2 = change;
                }

                for (int x = (int) x1; x < (int) x2; x++) {
                    double t = (x - x1) / (x2 - x1);
                    Vertex v = v12.mul(1 - t).add(v13.mul(t));

                    zBuffer.drawPixelWithTest(x, y, v.getPosition().getZ(), shader.shade(v));
                }
            }

            for (int y = (int) b.getY() + 1; y < (int) c.getY(); y++) {
                double s1 = (y - b.getY()) / (c.getY() - b.getY());
                double x1 = (b.getX() * (1 - s1) + c.getX() * s1);

                Vertex v23 = v2.mul(1 - s1).add(v3.mul(s1));

                double s2 = (y - a.getY()) / (c.getY() - a.getY());
                double x2 = (a.getX() * (1 - s2) + c.getX() * s2);

                Vertex v13 = v1.mul(1 - s2).add(v3.mul(s2));


                if (x1 > x2) {
                    double change = x1;
                    x1 = x2;
                    x2 = change;
                }

                for (int x = (int) x1; x < (int) x2; x++) {
                    double t = (x - x1) / (x2 - x1);
                    Vertex v = v23.mul(1 - t).add(v13.mul(t));

                    zBuffer.drawPixelWithTest(x, y, v.getPosition().getZ(), shader.shade(v));
                }
            }
        } else {
            lineRasterizer.rasterize(v1,v2);
            lineRasterizer.rasterize(v2,v1);
            lineRasterizer.rasterize(v3,v1);
        }
    }

    public void setFill(boolean fill) {
        this.fill = fill;
    }
}
