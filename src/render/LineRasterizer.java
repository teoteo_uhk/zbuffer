package render;

import model.Shader;
import model.Vertex;
import raster.ZBufferVisibility;
import transforms.Col;
import transforms.Vec3D;

import java.awt.*;

public class LineRasterizer {
    private ZBufferVisibility zBuffer;
    private int width, heigth;

    public LineRasterizer(ZBufferVisibility zBuffer) {
        this.zBuffer = zBuffer;
        this.width = zBuffer.getiBuffer().getWidth();
        this.heigth = zBuffer.getiBuffer().getHeight();
    }

    public void rasterize(Vertex v1, Vertex v2) {
        if(v1.getPosition().getZ() < 0 || v2.getPosition().getZ() < 0){
            return;
        }

        Vec3D a = v1.getPosition().ignoreW()
                .mul(new Vec3D(1, -1, 1))
                .add(new Vec3D(1, 1, 0))
                .mul(new Vec3D((width - 1) / 2., (heigth - 1) / 2., 1));

        Vec3D b = v2.getPosition().ignoreW()
                .mul(new Vec3D(1, -1, 1))
                .add(new Vec3D(1, 1, 0))
                .mul(new Vec3D((width - 1) / 2., (heigth - 1) / 2., 1));

        int x1 = (int) a.getX();
        int y1 = (int) a.getY();
        double z1 = a.getZ();

        int x2 = (int) b.getX();
        int y2 = (int) b.getY();
        double z2 = b.getZ();

        float k = ((float) y2 - (float) y1) / ((float) x2 - (float) x1);
        float q = y1 - k * x1;

        if (Math.abs(y2 - y1) <= Math.abs(x2 - x1)) {
            if (x1 <= x2) {
                for (int x = x1; x <= x2; x++) {
                    int y = (int) (k * x + q);

                    double t = ((double) x - x1) / (x2 - x1);
                    double zb = z2 * (1.0 - t) + z1 * t;
                    zBuffer.drawPixelWithTest(x, y, zb, v1.getColor());
                }
            } else {
                for (int x = x2; x <= x1; x++) {
                    int y = (int) (k * x + q);

                    double t = ((double) x - x2) / (x1 - x2);
                    double zb = z1 * (1.0 - t) + z2 * t;
                    zBuffer.drawPixelWithTest(x, y, zb, v2.getColor());
                }
            }
        } else {
            if (y1 <= y2) {
                for (int y = y1; y <= y2; y++) {
                    int x = (int) ((y - q) / k);

                    double t = ((double) y - y1) / (y2 - y1);
                    double zb = z1 * (1.0 - t) + z2 * t;

                    zBuffer.drawPixelWithTest(x, y, zb, v1.getColor());
                }
            } else {
                for (int y = y2; y <= y1; y++) {
                    int x = (int) ((y - q) / k);

                    double t = ((double) y - y2) / (y1 - y2);
                    double zb = z2 * (1.0 - t) + z1 * t;

                    zBuffer.drawPixelWithTest(x, y, zb, v2.getColor());
                }
            }
        }
    }
}
