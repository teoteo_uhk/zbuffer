package model;

import transforms.*;

public class Vertex {
    private Point3D position;
    private Col color;
    private Vec2D textCoords;
    private double one;

    public Vertex(double x, double y, double z, Col color) {
        this.position = new Point3D(x, y, z);
        this.color = color;
        this.textCoords = new Vec2D(color.getR(), color.getB());
        this.one = 1;
    }

    public Vertex (Point3D p, Col col, Vec2D textCoords){
        this.position = p;
        this.color = col;
        this.textCoords = textCoords;
    }
    public Point3D getPosition() {
        return position;
    }

    public Col getColor() {
        return color;
    }

    public Vec2D getTextCoords() {
        return textCoords;
    }

    public Vertex mul(double d){
        return new Vertex(position.mul(d), color.mul(d), textCoords.mul(d));
    }

    public Vertex mul(Mat4 mat, double d){
        return new Vertex(position.mul(mat), color.mul(d), textCoords.mul(d));
    }

    public Vertex mul(Vec3D v) {
        return new Vertex(new Point3D(position.getX() * v.getX(), position.getY() * v.getY(), position.getZ() * v.getZ()), color, textCoords);
    }

    public Vertex dehomog(double w){
        return new Vertex(position.getX()/w, position.getY()/w, position.getZ()/w, color);
    }

    public void setPosition(Point3D position) {
        this.position = position;
    }

    public Vertex add(Vertex v){
        return new Vertex(
                position.add(v.getPosition()),
                color.add(v.getColor()),
                textCoords.add(v.getTextCoords())
        );
    }

    public Vertex add(Vec3D v) {
        return new Vertex(new Point3D(position.getX() + v.getX(), position.getY() + v.getY(), position.getZ() + v.getZ()), color, textCoords);
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "position=" + position +
                '}';
    }
}
