package model;

public enum TopologyType {
    POINTS, LINES, LINES_STRIP, LINES_LOOP, TRIANGLES, TRINAGLES_STRIP
}
