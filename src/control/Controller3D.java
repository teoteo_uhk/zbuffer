package control;

import engine.TransformEngine;
import model.Shader;
import model.ShaderFullColor;
import model.ShaderInterpolation;
import model.Vertex;
import raster.ImageBuffer;
import raster.ZBufferVisibility;
import render.LineRasterizer;
import render.Renderer;
import render.TriangleRasterizer;
import solids.*;
import transforms.*;
import view.Panel;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Controller3D implements Controller {

    private final Panel panel;

    private int width, height, x, y, active = 0;
    private boolean fill = true;
    private boolean pressed = false;
    private boolean auto = false;
    private int ox, oy;
    private Camera camera = new Camera()
            .withPosition(new Vec3D(10,0,0))
            .withAzimuth(Math.PI)
            .withZenith(0)
            .withFirstPerson(true);

    private ZBufferVisibility zBufferVisibility;
    private TriangleRasterizer triangleRasterizer;
    private LineRasterizer lineRasterizer;

    private Cube cube = new Cube();
    private Pyramid pyramid = new Pyramid();
    private Axes axes = new Axes();
    private Curve curve = new Curve();

    private TransformEngine transformEngine = new TransformEngine(cube);

    private Mat4 projection = new Mat4OrthoRH(2, 2, 0.1, 200);

    private Renderer renderer;

    public Controller3D(Panel panel) {
        this.panel = panel;
        initObjects(panel.getRaster());
        initListeners(panel);
        redraw();
    }

    public void initObjects(ImageBuffer raster) {
        raster.setClearValue(new Col(0x101010));
        zBufferVisibility = new ZBufferVisibility(panel.getRaster());

        Shader shaderFullColor = v -> {
            return new Col(0, 1.0, 0);
        };

        Shader shaderInterpolation = v -> {
            return v.getColor();
        };

        lineRasterizer = new LineRasterizer(zBufferVisibility);
        triangleRasterizer = new TriangleRasterizer(zBufferVisibility, shaderInterpolation, lineRasterizer);

        renderer = new Renderer(triangleRasterizer, lineRasterizer, zBufferVisibility);
    }

    @Override
    public void initListeners(Panel panel) {
        initMouse();
        initKeys();
    }

    public void initMouse(){
        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    x = e.getX();
                    y = e.getY();
                }
            }
        });

        panel.addMouseMotionListener(new MouseMotionAdapter() {
            @Override public void mouseDragged(MouseEvent e) {
                int dX = e.getX() - x;
                int dY = e.getY() - y;
                camera = camera.addAzimuth(dX*Math.PI / width);
                camera = camera.addZenith(dY*Math.PI / height);
                x = e.getX();
                y = e.getY();
                redraw();
            }
        });
    }

    public void initKeys() {
        panel.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                double step = 0.1;
                super.keyPressed(e);
                switch (e.getKeyCode()) {
                    case (KeyEvent.VK_A): camera = camera.left(step); break;
                    case (KeyEvent.VK_D): camera = camera.right(step); break;
                    case (KeyEvent.VK_W): camera = camera.forward(step); break;
                    case (KeyEvent.VK_S): camera = camera.backward(step); break;
                    case (KeyEvent.VK_F):
                        if(fill) {
                            fill = false;
                            triangleRasterizer.setFill(fill);
                        } else {
                            fill = true;
                            triangleRasterizer.setFill(fill);
                        }
                        break;
                    case (KeyEvent.VK_C):
                        if(active > 2) {
                            active = 0;
                        }
                        active++;
                        changeObjects(active);
                        break;
                    case (KeyEvent.VK_Z): transformEngine.scale(1.2); break;
                    case (KeyEvent.VK_4): transformEngine.rotateX(0.5); break;
                    case (KeyEvent.VK_5): transformEngine.rotateY(0.5); break;
                    case (KeyEvent.VK_6): transformEngine.rotateZ(0.5); break;
                    case (KeyEvent.VK_P): projection = new Mat4PerspRH(Math.PI / 4, 1, 0.1, 200); break;
                    case (KeyEvent.VK_O): projection = new Mat4OrthoRH(2, 2, 0.1, 200); break;
                    case (KeyEvent.VK_I): automaticRotation(auto); break;
                    case (KeyEvent.VK_M): transformEngine.move(0,0.1,0.1); break;
                }
                redraw();
            };
        });
    }

    public void changeObjects(int active) {
        switch(active){
            case(1):
                transformEngine.setSolid(cube);
                break;
            case(2):
                transformEngine.setSolid(pyramid);
                break;
        }
    }

    private void redraw() {
        panel.clear();

        width = panel.getRaster().getWidth();
        height = panel.getRaster().getHeight();

        renderer.setViewMatrix(camera.getViewMatrix());
        renderer.setProjMatrix(projection);
        triangleRasterizer.setFill(fill);

        renderer.render(pyramid);
        renderer.render(curve);
        renderer.render(axes);
        renderer.render(cube);

        zBufferVisibility.clear();
        panel.repaint();
    }

    public void automaticRotation(boolean start) {
        Runnable automaticRotation = () -> {
            Thread.currentThread().setName("automaticRotation");
            try{
                while (start){
                    transformEngine.rotateX(0.5);
                    redraw();
                    TimeUnit.SECONDS.sleep(1);
                }
            } catch (Exception e){
                e.printStackTrace();
            }
        };
        Thread automaticRotationThread = new Thread(automaticRotation);
        automaticRotationThread.start();
    }

    private void hardClear() {
        panel.clear();
        initObjects(panel.getRaster());
    }

}
