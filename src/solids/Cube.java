package solids;

import model.Part;
import model.TopologyType;
import model.Vertex;
import transforms.Col;
import transforms.Point3D;

public class Cube extends Solid{
    public Cube() {
        getvB().add(new Vertex(0,0,1, new Col(255,0,0)));
        getvB().add(new Vertex(0,1,1, new Col(255,0,0)));
        getvB().add(new Vertex(1,1,1, new Col(255, 0,0)));
        getvB().add(new Vertex(1,0,1, new Col(0,255,0)));
        getvB().add(new Vertex(0,0,0, new Col(0,255,0)));
        getvB().add(new Vertex(0,1,0, new Col(0,255,0)));
        getvB().add(new Vertex(1,1,0, new Col(0,0,255)));
        getvB().add(new Vertex(1,0,0, new Col(0,0,255)));

        getiB().add(0);
        getiB().add(1);
        getiB().add(2);
        getiB().add(0);
        getiB().add(2);
        getiB().add(3);
        getiB().add(0);
        getiB().add(4);
        getiB().add(5);
        getiB().add(0);
        getiB().add(5);
        getiB().add(1);
        getiB().add(3);
        getiB().add(2);
        getiB().add(6);
        getiB().add(3);
        getiB().add(6);
        getiB().add(7);
        getiB().add(4);
        getiB().add(7);
        getiB().add(6);
        getiB().add(4);
        getiB().add(6);
        getiB().add(5);
        getiB().add(1);
        getiB().add(2);
        getiB().add(6);
        getiB().add(1);
        getiB().add(6);
        getiB().add(5);
        getiB().add(0);
        getiB().add(3);
        getiB().add(7);
        getiB().add(0);
        getiB().add(7);
        getiB().add(4);

        setUseModelMatrix(true);
        getpB().add(new Part(TopologyType.TRIANGLES, 0, 36));

    }
}
