package solids;

import model.Part;
import model.Vertex;
import transforms.Mat4;
import transforms.Mat4Identity;

import java.util.ArrayList;
import java.util.List;

public class Solid {
    private Mat4 modelMatrix = new Mat4Identity();
    private boolean useModelMatrix;

    public Mat4 getModelMatrix() {
        return modelMatrix;
    }

    public void setModelMatrix(Mat4 modelMatrix) {
        this.modelMatrix = modelMatrix;
    }

    public boolean isUseModelMatrix() {
        return useModelMatrix;
    }

    public void setUseModelMatrix(boolean useModelMatrix) {
        this.useModelMatrix = useModelMatrix;
    }

    // PartBuffer
    private List<Part> pB = new ArrayList<>();
    // VertexBuffer
    private List<Vertex> vB = new ArrayList<>();
    // IndexBuffer
    private List<Integer> iB = new ArrayList<>();

    public List<Part> getpB() {
        return pB;
    }

    public List<Vertex> getvB() {
        return vB;
    }

    public List<Integer> getiB() {
        return iB;
    }
}
