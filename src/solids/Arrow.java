package solids;

import model.Part;
import model.TopologyType;
import model.Vertex;
import transforms.Col;

public class Arrow extends Solid {

    public Arrow() {
        getvB().add(new Vertex(0,  0, 0, new Col(1., 0, 0)));
        getvB().add(new Vertex(.4, 0, 0, new Col(1., 0, 0)));
        getvB().add(new Vertex(.4, .5, 0, new Col(1., 0, 0)));
        getvB().add(new Vertex(.5,  0, 0, new Col(1., 0, 0)));
        getvB().add(new Vertex(.4,-.5, 0, new Col(1., 0, 0)));

        getiB().add(0);
        getiB().add(1);

        getiB().add(2);
        getiB().add(3);
        getiB().add(4);

        getpB().add(new Part(TopologyType.TRIANGLES, 0, 4));
    }
}
