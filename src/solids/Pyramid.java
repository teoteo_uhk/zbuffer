package solids;

import model.Part;
import model.TopologyType;
import model.Vertex;
import transforms.Col;
import transforms.Point3D;

public class Pyramid extends Solid {
    public Pyramid() {
        getvB().add(new Vertex(0.7, 0.7, 0, new Col(255, 1, 255)));
        getvB().add(new Vertex(1.2, 0.7, 0, new Col(255, 1, 255)));
        getvB().add(new Vertex(1.2, 1.2, 0, new Col(255, 1, 255)));
        getvB().add(new Vertex(0.7, 1.2, 0, new Col(255, 1, 255)));
        getvB().add(new Vertex(0.95, 0.95, 0.7, new Col(255, 1, 255)));

        getiB().add(0);
        getiB().add(1);
        getiB().add(1);
        getiB().add(2);
        getiB().add(2);
        getiB().add(3);
        getiB().add(3);
        getiB().add(0);

        getiB().add(4);
        getiB().add(0);
        getiB().add(4);
        getiB().add(1);
        getiB().add(4);
        getiB().add(2);
        getiB().add(4);
        getiB().add(3);

        setUseModelMatrix(true);
        getpB().add(new Part(TopologyType.LINES, 0, 8));
    }
}
