package solids;

import model.Part;
import model.TopologyType;
import model.Vertex;
import transforms.*;

public class Curve extends Solid {
    private Bicubic curve;
    private Mat4 type = Cubic.BEZIER;
    private int accuracy = 50;

    public Curve(){
        Point3D p00 = new Point3D(2, 0, 1);
        Point3D p01 = new Point3D(2, 1, 1);
        Point3D p02 = new Point3D(2,0, 0);
        Point3D p03 = new Point3D(2, 1, 1);
        Point3D p10 = new Point3D(0, 0, 1);
        Point3D p11 = new Point3D(2, 2, 2);
        Point3D p12 = new Point3D(1, 1, 1);
        Point3D p13 = new Point3D(2, 1, 1);
        Point3D p20 = new Point3D(2, 2, 2);
        Point3D p21 = new Point3D(2, 2, 1);
        Point3D p22 = new Point3D(2, 1, 1);
        Point3D p23 = new Point3D(3, 3, 2);
        Point3D p30 = new Point3D(2, 0, 1);
        Point3D p31 = new Point3D(2, 1, 1);
        Point3D p32 = new Point3D(2, 0, 0);
        Point3D p33 = new Point3D(2, 1, 1);

        curve = new Bicubic(type, p00, p01, p02, p03, p10, p11, p12, p13, p20, p21, p22, p23, p30, p31, p32, p33);

        for (int i = 0; i < this.accuracy; i++) {
            for (int j = 0; j < this.accuracy; j++) {
                Point3D point = curve.compute((double) i / this.accuracy, (double) j / this.accuracy);
                getvB().add(new Vertex(point.getX(), point.getY(), point.getZ(), new Col(255,0,0)));
            }
        }

        for (int i = 0; i < this.accuracy; i++) {
            for (int j = 0; j < this.accuracy; j++) {
                getiB().add(i * (this.accuracy + 1) + j);
                getiB().add(i * (this.accuracy + 1) + j + 1);
                getiB().add((i + 1) * (this.accuracy + 1) + j);

                getiB().add(i * (this.accuracy + 1) + j + 1);
                getiB().add((i + 1) * (this.accuracy + 1) + j);
                getiB().add((i + 1) * (this.accuracy + 1) + j + 1);
            }
        }

        setUseModelMatrix(true);
        // Pokus o vykreslení přes troúhelníky, bohužel to vždy při přiblížení spadne.
        // getpB().add(new Part(TopologyType.TRIANGLES, 0, 500));
        getpB().add(new Part(TopologyType.LINES, 0, 500));
    }
}
