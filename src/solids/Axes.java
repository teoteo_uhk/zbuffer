package solids;

import model.Part;
import model.TopologyType;
import model.Vertex;
import transforms.Col;

public class Axes extends Solid{
    public Axes() {
        getvB().add(new Vertex(0, 0, 0, new Col(255,1,255)));

        getvB().add(new Vertex(1, 0, 0, new Col(255,1,1)));
        // Šipka
        getvB().add(new Vertex(0.9, 0.05, 0, new Col(255,1,1)));
        getvB().add(new Vertex(0.9, -0.05, 0, new Col(255,1,1)));

        getvB().add(new Vertex(0, 1, 0, new Col(1,255,1)));
        // Šipka
        getvB().add(new Vertex(0.05, 0.9, 0, new Col(255,1,255)));
        getvB().add(new Vertex(-0.05, 0.9, 0, new Col(255,1,255)));

        getvB().add(new Vertex(0, 0, 1, new Col(1,1,255)));
        // Šipka
        getvB().add(new Vertex(0.05, 0.05, 0.9, new Col(1,255,255)));
        getvB().add(new Vertex(-0.05, -0.05, 0.9, new Col(1,255,255)));

        getiB().add(0);
        getiB().add(1);

        // Šipka
        getiB().add(1);
        getiB().add(2);

        getiB().add(1);
        getiB().add(3);
        //

        getiB().add(0);
        getiB().add(4);

        // Šipka
        getiB().add(4);
        getiB().add(5);

        getiB().add(4);
        getiB().add(6);
        //

        getiB().add(0);
        getiB().add(7);

        // Šipka
        getiB().add(7);
        getiB().add(8);

        getiB().add(7);
        getiB().add(9);
        //

        setUseModelMatrix(false);
        getpB().add(new Part(TopologyType.LINES, 0,9));
    }
}
