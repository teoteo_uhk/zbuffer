package raster;

public class DepthBuffer implements Raster<Double> {
    private double[][] buffer;
    private int width, height;
    private double clearValue = 1;

    public DepthBuffer(int width, int height) {
        this.buffer = new double[width][height];
        this.width = width;
        this.height = height;
    }

    @Override
    public void clear() {
        for(int x = 0; x < buffer.length; x++){
            for (int y = 0; y < buffer[x].length; y++) {
                this.buffer[x][y] = clearValue;
            }
        }
    }

    @Override
    public void setClearValue(Double value) {
        this.clearValue = value;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public Double getElement(int x, int y) {
        if(x >= 0 && x < width-1 && y >= 0 && y < height-1) {
            return this.buffer[x][y];
        }
        return clearValue;
    }

    @Override
    public void setElement(int x, int y, Double value) {
        this.buffer[x][y] = value;
    }

    public double getClearValue() {
        return clearValue;
    }
}
