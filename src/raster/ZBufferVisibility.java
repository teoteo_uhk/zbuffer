package raster;

import transforms.Col;

import java.util.ArrayList;

public class ZBufferVisibility {
    private ImageBuffer iBuffer;
    private DepthBuffer dBuffer;

    public ZBufferVisibility(ImageBuffer iBuffer) {
        this.iBuffer = iBuffer;
        this.dBuffer = new DepthBuffer(iBuffer.getWidth(), iBuffer.getHeight());
    }

    public void drawPixelWithTest(int x, int y, double z, Col color) {
        if(dBuffer.getElement(x, y) > z && z >= 0.0) {
            dBuffer.setElement(x, y, z);
            iBuffer.setElement(x, y, color);
        }
    }

    public ImageBuffer getiBuffer() {
        return iBuffer;
    }

    public DepthBuffer getdBuffer() {
        return dBuffer;
    }

    public void clear(){
        dBuffer.clear();
    }
}
