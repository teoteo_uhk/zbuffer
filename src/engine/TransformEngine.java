package engine;

import model.Vertex;
import solids.Solid;
import transforms.*;

public class TransformEngine {
    public Solid solid;

    public TransformEngine(Solid solid) {
        this.solid = solid;
    }

    public void setSolid(Solid solid) {
        this.solid = solid;
    }

    public void rotateX(Double alpha) {
        Mat4 model = solid.getModelMatrix();
        model = model.mul(new Mat4RotX(alpha));
        solid.setModelMatrix(model);
    }

    public void rotateY(Double alpha) {
        Mat4 model = solid.getModelMatrix();
        model = model.mul(new Mat4RotY(alpha));
        solid.setModelMatrix(model);
    }

    public void rotateZ(Double alpha) {
        Mat4 model = solid.getModelMatrix();
        model = model.mul(new Mat4RotZ(alpha));
        solid.setModelMatrix(model);
    }

    public void scale(double value) {
        Mat4 scale = new Mat4Identity();
        scale = scale.mul(new Mat4Scale(value));
        for (int i = 0; i < solid.getvB().size(); i++) {
            Vertex point = solid.getvB().get(i).mul(scale, 1);
            solid.getvB().set(i, point);
        }
    }

    public void move(double x, double y, double z) {
        Mat4 trans = new Mat4Identity();
        trans = trans.mul(new Mat4Transl(x,y,z));
        for (int i = 0; i < solid.getvB().size(); i++) {
            Vertex point = solid.getvB().get(i).mul(trans, 1);
            solid.getvB().set(i, point);
        }
    }

    public void moveScale(double x, double y, double z, double value){
        Mat4 trans = new Mat4Identity();
        trans = trans.mul(new Mat4Transl(x,y,z));

        Mat4 scale = new Mat4Identity();
        scale = scale.mul(new Mat4Scale(value));

        for (int i = 0; i < solid.getvB().size(); i++) {
            Vertex point = solid.getvB().get(i).mul(trans, 1).mul(scale, 1);
            solid.getvB().set(i, point);
        }
    }
}
